Project:


**BOT for sending JOB APPLICATIONS**

The goal of this library is to automatically send applications to jobs, via StackOverflow website.
In order to do so, the user specifies, via config.json, his/her email, job keywords and job location,
together with an application letter.
