from jobs.StackJobs import JobsBot

if __name__ == "__main__":
    try:
        sj_bot = JobsBot()
        sj_bot.do_it()
    except KeyboardInterrupt:
        print("program execution interrupted.\nExiting.")
