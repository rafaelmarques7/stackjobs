# import libraries
import os
import time
import json
import itertools
from selenium import webdriver

#___________________________________________________________
# Static functions


def get_path_from_rel(rel_path):
    cwd = os.getcwd()
    return os.path.join(cwd, rel_path)


def get_configs():
    config_path = get_path_from_rel("configs.json")
    with open(config_path, 'r') as config_json:
        config_data = json.load(config_json)
    return config_data


def yield_base_url(keywords, locations):
    for kwd, loc in itertools.product(keywords, locations):
        yield "https://stackoverflow.com/jobs?sort=i&q={}&l={}&mxs=MidLevel".format(kwd, loc)


def add_to_taboo(taboo_path, url):
    with open(taboo_path, 'a+') as f:
        f.write(url + "\n")


def is_taboo(taboo_path, url):
    """ returns True if url is in Tabu list """
    with open(taboo_path, 'r') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    if url in content:
        return True
    return False

# ___________________________________________________________
# Class definition


class JobsBot(object):
    def __init__(self):
        """ initiates the bot by loading config variables, and driver """
        self.configs = get_configs()
        self.taboo_path = get_path_from_rel(self.configs["taboo_rel_path"])
        self.driver = webdriver.Chrome()

    def nav_to(self, url, sleep_time=2):
        """ navigates toa given page and waits for a predefined time """
        self.driver.get(url)
        time.sleep(sleep_time)

    def get_target_links(self, url):
        targets = []
        while True:
            self.nav_to(url)
            self.get_targets_from_page(targets)
            url = self.check_next_page()
            if not url:
                break
        return targets

    def get_targets_from_page(self, targets):
        """ gets all job offer links from the current page """
        for num in range(1, 26):
            xpath = '//*[@id="mainbar"]/div[2]/div/div[{}]/div[3]/h2/a'.format(num)
            target_elems = self.driver.find_elements_by_xpath(xpath)
            if len(target_elems) == 0:
                continue
            targets.append(str(target_elems[0].get_attribute("href")))

    def check_next_page(self):
        """ verifies if there is a next page to navigate to. returns the page url, if there is. return False otherwise """
        next_elems = self.driver.find_elements_by_link_text("nextchevron_right")
        if len(next_elems) == 0:
            return False
        else:
            url = next_elems[0].get_attribute("href")
            return url

    def apply_to(self, url):
        """ navigates to target page, verifies if application is possible, uploads info, and clicks apply job """
        self.nav_to(url)
        can_apply = self.nav_to_apply()
        if not can_apply:
            return
        self.upload_all()
        # self.click_apply()

    def click_apply(self):
        """ clicks on the 'apply' button """
        xpath_apply = '//*[@id="content"]/div[3]/form/div[5]/div[1]/input'
        self.driver.find_element_by_xpath(xpath_apply).click()

    def nav_to_apply(self):
        """ navigates to application page, is possible, and returns True. Returns False, is not possible """
        xpath_apply = '//*[@id="content"]/header/div[3]/div[1]/a'
        apply_el = self.driver.find_element_by_xpath(xpath_apply)
        is_external = apply_el.get_attribute("target")
        if is_external:
            return False
        apply_el.click()
        return True

    def upload_all(self, sleep_time=2):
        self.upload_cv()
        self.upload_base_info()
        self.upload_letter()
        time.sleep(sleep_time)

    def upload_base_info(self):
        self.driver.find_element_by_id("CandidateName").send_keys(self.configs["candidate_name"])
        self.driver.find_element_by_id("CandidateLocation").send_keys(self.configs["candidate_location"])
        self.driver.find_element_by_id("CandidateEmail").send_keys(self.configs["candidate_email"])

    def upload_letter(self):
        xpath_text = '//*[@id="CoverLetter"]'
        text_el = self.driver.find_element_by_xpath(xpath_text)
        self.driver.execute_script("arguments[0].value = arguments[1]", text_el, self.configs["letter"])

    def upload_cv(self):
        # cvr_path = "C:\\Users\\Y\\Desktop\\bfly_startup\\bfly_reuniao\\cvr.pdf"
        cwd = os.getcwd()
        cvr_path = os.path.join(cwd, self.configs["cv_rel_path"])
        xpath_dd = '//*[@id="uploader-wrapper"]/div/p[2]/a/input'
        self.driver.find_element_by_xpath(xpath_dd).send_keys(cvr_path)

    def do_it(self):
        keywords, locations = self.configs["keywords"], self.configs["locations"]
        for base_url in yield_base_url(keywords, locations):
            self.nav_to(base_url)
            targets = self.get_target_links(base_url)
            for target in targets:
                if is_taboo(self.taboo_path, target):
                    continue
                self.apply_to(target)
                add_to_taboo(self.taboo_path, target)




